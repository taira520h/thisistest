CREATE TABLE users (
  user_id uuid not null primary key,
  name varchar
)

INSERT INTO users VALUES ( uuid_generate_v4(), 'Hello Monday')
INSERT INTO users VALUES ( uuid_generate_v4(), 'testman')

DROP TABLE users;
