require('dotenv').config();
const pgp = require('pg-promise')({})

const CN = process.env.DATABASE_URL
const db = pgp(CN)

module.exports = db;
